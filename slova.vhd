library ieee;
use ieee.std_logic_1164.all;

entity slova is
    port (
        btn_left, btn_right, btn_up, btn_down, btn_center: in std_logic;
	rs232_tx: out std_logic;
	clk_25m: in std_logic;
	sw: in std_logic_vector(3 downto 0); 
        led: out std_logic_vector(7 downto 0)
    );
end slova;


architecture behavioral of slova is
	signal code: std_logic_vector(7 downto 0);
	signal code_slovo: std_logic_vector(7 downto 0);
	signal code_brojka: std_logic_vector(7 downto 0);
	signal btns: std_logic_vector(4 downto 0);

begin

btns <= btn_down & btn_left & btn_center & btn_up & btn_right;
broj: entity brojke port map (
		 lp_left => btn_left, lp_right => btn_right, lp_up => btn_up, lp_down => btn_down, lp_center => btn_center, lp_code => code_brojka
	);
	
with btns select
		    code_slovo <=
			"00000000" when "00000", -- ASCII NULL
			"00000000" when "10000",
			"01001100" when "01000",
			"01100101" when "00100",
			"01101111" when "00010",
			"01010000" when "00001",
			"01101100" when "11000",
			"01100101" when "10100",
			"01110011" when "10010",
			"01100101" when "10001",
			"--------" when others;
			
with sw(0) select
	code <= code_slovo when '0',
		code_brojka when '1',
		"--------" when others;


led <= code;

    serializer: entity serial_tx port map (
	clk => clk_25m, byte_in => code, ser_out => rs232_tx
    );
    
end behavioral;
    