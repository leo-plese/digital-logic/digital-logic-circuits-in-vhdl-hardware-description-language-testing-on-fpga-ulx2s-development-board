library ieee;
use ieee.std_logic_1164.all;

entity brojke is port (
        lp_left, lp_right, lp_up, lp_down, lp_center: in std_logic; --ulazi
	lp_code: out std_logic_vector(7 downto 0) --izlaz
    );
end brojke;

architecture behavioral of brojke is
	signal code_brojka: std_logic_vector(7 downto 0);
	signal btns: std_logic_vector(4 downto 0);
	
begin

btns <= lp_down & lp_left & lp_center & lp_up & lp_right;

with btns select
    code_brojka <=
    	"00000000" when "00000",
	"00000000" when "10000",
	"00110011" when "01000",
	"00110110" when "00100",
	"00110101" when "00010",
	"00110000" when "00001",
	"00110101" when "11000",
	"00110110" when "10100",
	"00110111" when "10010",
	"00110000" when "10001",
	"--------" when others;
	
lp_code <= code_brojka;

end behavioral;
